# Prosjekt - DCSG1005

[[_TOC_]]

# Introduksjon av sårbarhet

I dette prosjektet skal det forsøkes å utnyte active directory som ikke er sikret ordentlig. Det blir først utført ett *reconnaissance*-angrep for å samle informasjon om domenet som skal komprimeres ved hjelp av verktøyet *bloodhound*.

Deretter skal det bli utført et *Post-exploitation*-angrep ved hjelp av verktøyet mimikatz for å hovedsakelig utføre pass-the-hash. Dette utnytter sårbarheter av brukere som ikke er beskyttet tilstrekkelig slik at det er mulig å hente ut informasjon som hjelper med å utføre angrepet. Mimikatz utnytter kjente sårbarheter i windows. En annen sårbarhet som også blir utforsket ved bruk av mimikatz er *AMSI buypassing*, hvor det er mulig å komme seg forbi *Anti Malware Scan Interface*, som ellers ville nektet en nedlasting av mimikatz. 

# Oppsett av infrastruktur
For å sette opp Domenen før angrepet ble det lagd ulike brukere, grupper, maskiner og grupperegler i active directory. Mesteparten av disse ble generert av verktøyet [BadBlood](https://github.com/davidprowe/BadBlood).

*Figur 1.1 viser at det har blitt generert 3246 brukere, 260 grupper og 57 maskiner i active directory*

![Figur 1.1](https://gitlab.com/markri325/infrastruktur-prosjekt/-/blob/master/Bilder/setup4.PNG)

I tillegg ble det lagd 2 grupper, *My RDP Users* og *My Remote Administrators*. *My RDP Users* ble deretter lagt til i den lokale *Remote Desktop Users* gruppen på WORKSTATIONS og *My Remote Administrators* på SERVERS. Det ble også lagd to GPO'er, *MyPSRemote* som ble lagt til i SERVERS, og *MyRDP* som ble lagt til i WORKSTATIONS. *MyRDP* ble også kopiert til en ny GPO, MyRDPServers som ble lagt til i SERVERS. OU'en WORKSTATIONS inneholder CL1 og OU'en SERVERS inneholder SRV1 og SRV2. Dette gjør at medlemmer av *My RDP Users* og *My Remote Administrators* har mulighet til å benytte seg av RDP og PSRemoting på CL1, SRV1 eller SRV2.

*Figur 1.2 og 1.3 viser gruppene My RDP User og My Remote Administrators, og GPO'ene MyPSRemote og MyRDP*

![Figur 1.2](https://gitlab.com/markri325/infrastruktur-prosjekt/-/blob/master/Bilder/setup5.PNG)

![Figur 1.3](https://gitlab.com/markri325/infrastruktur-prosjekt/-/blob/master/Bilder/setup6.PNG)

En bruker, Petter Northug, ble også lagt til som er en lokal bruker på SRV1. PNorthug har full eierskap og ingen andre enn han kan se passordet hans, så en angriper vil ikke kunne finne passordet med mindre det er meget svakt. Det ble så lagd en RDP fil for PNorthug sin legitimasjon. Til slutt ble PNorthug lagt til i gruppene *My RDP Users* og *My Remote Administrators* slik at han skal kunne benytte seg av RDP og PSRemoting mellom SRV1 og CL1.

*Figur 1.4 viser brukeren PNorthug og gruppene han tilhører*

![Figur 1.4](https://gitlab.com/markri325/infrastruktur-prosjekt/-/blob/master/Bilder/setup7.PNG)

# Angrep på infrastruktur
Angrepet i dette prosjektet tar utgangspunkt i at en trusselaktør har fått tilgang til CL1 som lokal administrator, så skal han jobbe seg fra CL1 som lokal administrator til domene administrator. Vi tar også utgangspunkt i at trusselaktøren har fått litt hjelp fra brukeren *mysil* som har sendt trusselaktøren en mappe med all informasjon om RESKIT domenet. Dette er data som ikke inneholder noe klassifisert informasjon, men det hjelper trusselaktøren med å få en oversikt over domenet med verktøyet *BloodHound* som er et graf-verktøy som illustrerer sammenhenger mellom ulike maskiner, brukere, grupper, etc på domenet.

Trusselaktøren laster da opp denne mappen i Bloodhound og velger queryen *Find Shortest Paths to Domain Admins*.

*Figur 2.1 viser hele AD-strukturen til RESKIT domenen i verktøyet BloodHound*

![Figur 2.1](https://gitlab.com/markri325/infrastruktur-prosjekt/-/blob/master/Bilder/Skjermbilde.PNG)

Som vist i *figur 2.1* så er RESKIT.ORG et nokså stort domene, så det gjør at det blir litt vanskelig å vite hvor man skal starte får å gå videre for å komprimere domenet, men det gir likevel en god oversikt over de ulike objektene på domenet og relasjonene mellom dem. 

Etter å ha studert strukturen og noen av gruppene finner trusselaktøren en bruker som er av interesse, PNorthug. Trusselaktøren bruker da bloodhound til å se PNorthugs relasjoner med ulike grupper i strukturen.

*Figur 2.2 viser PNorthugs relasjon til ulike grupper i strukturen*

![Figur 2.2](https://gitlab.com/markri325/infrastruktur-prosjekt/-/blob/master/Bilder/bloodhound.PNG)

Utifra gruppene PNorthug er medlem av, ser trusselaktøren at PNorthug kan bruke rdp til å koble seg til andre maskiner. Trusselaktøren har hittil kun tilgang som lokal administrator på CL1, men han vet at PNorthug egentlig ligger på SRV1 men bruker rdp for å koble seg til CL1. Trusselaktøren kan altså komme seg inn på SRV1 hvis han klarer å komprimere PNorthug.

Ved hjelp av Mimikatz på CL1 kan trusselaktøren hente ut passordet til PNorthug.

For å omgå Windows Defender sin AMSI modul som forbyr visse stringer å lastes ned, kan man splitte meldingen opp i flere mindre biter. 

Trusselaktøren logger først inn som local admin på CL1 og oppretter en mappe i temporary mappen til local admin. 
Ved hjelp av funksjonen *Add-MpPreference -ExclusionPath $MpExclusionPath* blokkere local admin tilgangen Windows Defender har i mappen.
Så laster trusselaktøren ned Mimikatz i mappen den nylig opprettet.

*Figur 2.3 viser kommandoene brukt for å unngå Windows Defender og laste ned Mimikatz*

![Figur 2.3](https://gitlab.com/markri325/infrastruktur-prosjekt/-/blob/master/Bilder/Angrep2.PNG)


Ved hjelp av Mimikatz kan trusselaktøren logge og modifisere andres brukeres data. 
For å senere koble PNorthug til SRV1 må man tillate at flere RDP sessions kjører samtidig.
Dette kan tillates fra Mimikatz. *Se Figur 2.4*

![Figur 2.4](https://gitlab.com/markri325/infrastruktur-prosjekt/-/blob/master/Bilder/Angrep1.PNG)


Ved å bruke logging funksjonene til Mimikatz henter trusselaktøren ut en hel data om brukere han er interessert i, og ved hjelp av 
powershellfunksjonen `Select-String "Select-String -Path .\pentest.log -Pattern '[Nn]ame(\s)*:(\s)*PNorthug' -Context 2,5"`, kan han filtrere ut uønsket data fra søket.
*Se Figur 2.5*

![Figur 2.5](https://gitlab.com/markri325/infrastruktur-prosjekt/-/blob/master/Bilder/Angrep3.PNG)


Mimikatz har en funksjon kalt pass-the-hash som kan impersonere en bruker  til en remote server ved hjelp av en NTLM hash, og det er nettopp dette trusselaktøren utnytter. Ved å bruke NTLM hashen aktøren fant i forrige figur får han impersonert PNorthug.
*Figur 2.6 viser kommandoen brukt for å impersonere PNorthug, og resulatetet av neste kommando.*

![Figur 2.6](https://gitlab.com/markri325/infrastruktur-prosjekt/-/blob/master/Bilder/Angrep4.PNG)


Etter at trusselaktøren klarte å impersonere PNorthug kan man se på Fig. 2.6 at han kjører en kommando som Powershell
og starter en PsSession i SRV1, hvor han enkelt kan finne innholdet i filen *pass.txt*

Det må sies at passordet ikke var Passord123, men det ble byttet etter at det ble funnet så det skulle bli lettere å logge seg inn som PNorthug. 

![Figur 2.7](https://gitlab.com/markri325/infrastruktur-prosjekt/-/blob/master/Bilder/mimikatz.png)

*Figur 2.7 viser det opprinnelige passordet til PNorthug*

Etter dette har trusselaktøren fått tilgang til alt PNorthug har, blant annet SRV1 og CL1.

# Var angrepet vellykket?
Angrepet var nokså vellykket. Angriperen fikk tak i passordet til PNorthug, og klarte deretter å koble seg til SRV1. Det faktum at angrepet var vellykket avhenger likevel av flere faktorer som måtte være til stede som ikke er en selvfølge hvis ikke prosjektet var satt opp som det var. Angriperen fikk til alt som skulle gjøres, men det måtte nok ha blitt gjort mer fra angriperen sin side hvis prosjektet ikke hadde vært satt opp sånn som det var.

Angriperen hadde allerede tilgang som lokal administrator på CL1, noe som ikke blir demonstrert hvordan angriperen har fått til. I dette tilfellet starter bare angrepet med at han har tilgang som lokal administrator på CL1. 

En annen faktor er at angriperen som lokal administrator ikke har direkte mulighet til å kjøre sharphound på domenet. Derfor ble scenarioet lagt opp til at angriperen fikk hjelp fra en annen aktør som sendte mappen som ble lagd av sharphound. 

I tillegg vet ikke angriperen når PNorthug vil koble seg til CL1 med rdp. Angriperen kan se hvem som er koblet til CL1 ved å kjøre kommandoen ts:list, men det er ikke gitt at PNorthug er koblet til på samme tidspunkt som angriperen er avhengig av det, så da må angriperen eventuelt vente til at PNorthug har koblet seg til.

Opprinnelig var angrepet vellykket når angriperen fikk tak i passordet til PNorthug. Det ble likevel gjort et forsøk på å generere en golden ticket til PNorthug slik at han skulle få domene administrator rettigheter, men det var desverre ikke velykket.

*Figur 3.1 viser error ved forsøk på å generere golden ticket*

![Figur 3.1](https://gitlab.com/markri325/infrastruktur-prosjekt/-/blob/master/Bilder/error.png)

Siden golden tickets ikke var en del av prosjektet så konkluderes det fortsatt med at angrepet var vellykket.

# Hvordan forsvare mot angrepet

## Bedre AD Struktur
Når den ansvarlige for oppsettet av infrastrukturen i dette environmentet satte opp infrastrukturen gjorde han noen store feil som 
Som tidligere nevnt var det mange faktorer som gjorde angrepet til trusselaktøren vellykket. 
Ved å gjøre et par endringer i bl. a. GPO vil nettverket være mye vanskeligere å angripe. Når man fordeler ut rettigheter til forskjellige brukere er det viktig å ha bregrepet JEA (Just Enough Access) i bakhodet.
Dette innebærer å gi de aller fleste brukere så lite rettigheter som mulig, og heller gi de flere rettigheter senere når man finner ut at de trenger det, og evt. fjerne de etterpå.
Et annet sikkerhetstiltak er å legge brukere i 'Protected Users' gruppen, som vi kommer tilbake til senere i rapporten. 



## Oppdage bruk av mimikatz

Windows *System Monitor(sysmon)* er ett system som logger og overvåker system-aktivitet i *Windows Event Log*[^1]. Siden mimikatz lar brukere dumpe legitimasjan i Windows med lsass-prossesen, så er sysmon ett glimrende verktøy for å oppdage bruk av mimikatz[^2]. Man kan da altså konfigurere sysmon til å se etter om mimikatz aksesserer lsass-prosessen, noe som gjør det mulig å fort oppdage om noen bruker mimikatz på et system.

## Mimikatz hardening

### Protected Users
‘Protected users’ er en standard sikkerhets gruppe i Windows som tvinger brukerene i gruppen til å autentisere med domenetet via Kerberos[^3]. Dette hjelper med å forhindre lekasje av passord hasher. For at dette skal være et effektivt forsvar mot Mimikatz angrep må alle aktuelle brukere være i gruppen, så det krever større kontroll over infrastrukturen enn andre tiltak. Brukeren PNorthug ble lagt til denne gruppen med kommandoen

`Add-ADGroupMember –Identity 'Protected Users' –Members PNorthug`


### Credential Caching
For å forhindre at Mimikatz skal kunne ta en passord hash for å impersonere brukeren kan man stoppe Windows fra å lagre passord hasher. Windows lagrer vanligvis de 10 nyligste brukte hashene i denne cachen, men det går ann å ender antall til 0. Dette kan man gjøre ved hjelp av ‘Local Security Policy’ og stien vist under:

`Local Security Policy -> Local Policies -> Security Options -> Interactive Logon: Number of previous logons to cache -> 0`

Å endre antallet til 0 stopper Windows fra å lagre passord der, som forhindrer Mimikatz fra å bruke dem[^3]. 


# Egenvurdering

I starten av prosjektet var det litt uklarhet om hvordan prosjektet skulle bli gjennomført, men ting falt litt på plass mens vi jobbet med det. I starten var planen å kun bruke bloodhound, men vi syns det ble litt tynt så vi bestemte oss for å utføre pass-the-hash angrep på domene brukere for å kunne få tilgang til andre maskiner de brukerene evt tilhører. Når vi bestemte oss for å bruke mimikatz var først planen å få tak i domeneadministrator passordet, men det endret seg til at målet ble å få tilgang til domenebrukeren PNorthug. Når vi hadde fullført angrepet på Pnorthugh ble det også forsøkt å generere en *Golden Ticket* til han, men det fungerte ikke.

Det var altså litt uoversiktelig i starten av prosjektet, men når ting falt på plass så føler vi at det ble ganske bra. Sammarbeidet i gruppen fungerte bra, og medlemmene gjorde det vi skulle gjøre i god tid og vi hadde regelmessige møter hvor vi jobbet sammen. Alle medlemmene bidro godt med gjennomførelsen av selve angrepet og alle bidro godt på å skrive rapporten. Alle medlemmene var også flinke til å logge timene vi brukte under prosjektet.

Hvis det er noe vi skulle gjort bedre så hadde det kanskje vært å bruke litt mer tid på å planlegge hva slags sårbarhet vi skulle utforske før vi startet med angrepet. Vi føler likevel at prosjektet var godt gjennomført siden ting falt på plass underveis.

# Referanser
[^1]: https://docs.microsoft.com/en-us/sysinternals/downloads/sysmon
[^2]: https://medium.com/@levurge/detecting-mimikatz-with-sysmon-f6a96669747e
[^3]: https://medium.com/blue-team/preventing-mimikatz-attacks-ed283e7ebdd5

Compendia: https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.md

Vi brukte gitlab til prosjektet. Url til gitlab-repo: https://gitlab.com/markri325/infrastruktur-prosjekt

Timelogging:
- Halfdan Moesgaard Skjesol

![Halfdan](https://gitlab.com/markri325/infrastruktur-prosjekt/-/blob/master/Bilder/logghalfdan.PNG)

- Martin Kristensen Eide

![Martin](https://gitlab.com/markri325/infrastruktur-prosjekt/-/blob/master/Bilder/loggmartin.PNG)

- Anders Slaaen

![Anders](https://gitlab.com/markri325/infrastruktur-prosjekt/-/blob/master/Bilder/logganders.PNG)
